require 'spec_helper'

describe "interviewees/index.html.erb" do
  before(:each) do
    assign(:interviewees, [
      stub_model(Interviewee,
        :name => "Name",
        :email => "Email"
      ),
      stub_model(Interviewee,
        :name => "Name",
        :email => "Email"
      )
    ])
  end

  it "renders a list of interviewees" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Email".to_s, :count => 2
  end
end
