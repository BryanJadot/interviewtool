require 'spec_helper'

describe "interviewees/new.html.erb" do
  before(:each) do
    assign(:interviewee, stub_model(Interviewee,
      :name => "MyString",
      :email => "MyString"
    ).as_new_record)
  end

  it "renders new interviewee form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => interviewees_path, :method => "post" do
      assert_select "input#interviewee_name", :name => "interviewee[name]"
      assert_select "input#interviewee_email", :name => "interviewee[email]"
    end
  end
end
