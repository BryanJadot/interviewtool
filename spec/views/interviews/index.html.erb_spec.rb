require 'spec_helper'

describe "interviews/index.html.erb" do
  before(:each) do
    assign(:interviews, [
      stub_model(Interview,
        :interviewer_id => 1,
        :interviewee_id => 1,
        :time_start => "",
        :chat => "Chat",
        :notes => "Notes"
      ),
      stub_model(Interview,
        :interviewer_id => 1,
        :interviewee_id => 1,
        :time_start => "",
        :chat => "Chat",
        :notes => "Notes"
      )
    ])
  end

  it "renders a list of interviews" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Chat".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Notes".to_s, :count => 2
  end
end
