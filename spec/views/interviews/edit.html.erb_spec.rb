require 'spec_helper'

describe "interviews/edit.html.erb" do
  before(:each) do
    @interview = assign(:interview, stub_model(Interview,
      :interviewer_id => 1,
      :interviewee_id => 1,
      :time_start => "",
      :chat => "MyString",
      :notes => "MyString"
    ))
  end

  it "renders the edit interview form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => interviews_path(@interview), :method => "post" do
      assert_select "input#interview_interviewer_id", :name => "interview[interviewer_id]"
      assert_select "input#interview_interviewee_id", :name => "interview[interviewee_id]"
      assert_select "input#interview_time_start", :name => "interview[time_start]"
      assert_select "input#interview_chat", :name => "interview[chat]"
      assert_select "input#interview_notes", :name => "interview[notes]"
    end
  end
end
