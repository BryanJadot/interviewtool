require 'spec_helper'

describe "questions/new.html.erb" do
  before(:each) do
    assign(:question, stub_model(Question,
      :interviewer_id => 1,
      :question => "MyString",
      :codedoc => "MyString"
    ).as_new_record)
  end

  it "renders new question form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => questions_path, :method => "post" do
      assert_select "input#question_interviewer_id", :name => "question[interviewer_id]"
      assert_select "input#question_question", :name => "question[question]"
      assert_select "input#question_codedoc", :name => "question[codedoc]"
    end
  end
end
