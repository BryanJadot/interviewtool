require "spec_helper"

describe InterviewersController do
  describe "routing" do

    it "routes to #index" do
      get("/interviewers").should route_to("interviewers#index")
    end

    it "routes to #new" do
      get("/interviewers/new").should route_to("interviewers#new")
    end

    it "routes to #show" do
      get("/interviewers/1").should route_to("interviewers#show", :id => "1")
    end

    it "routes to #edit" do
      get("/interviewers/1/edit").should route_to("interviewers#edit", :id => "1")
    end

    it "routes to #create" do
      post("/interviewers").should route_to("interviewers#create")
    end

    it "routes to #update" do
      put("/interviewers/1").should route_to("interviewers#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/interviewers/1").should route_to("interviewers#destroy", :id => "1")
    end

  end
end
