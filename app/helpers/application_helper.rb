module ApplicationHelper

  # Return the logo
   def logo
      image_tag("logo.png", :alt => "Interviewtool", :class => "round")
   end

  # Return a title on a per-page basis.
  def title
    base_title = "Interviewtool"
    if @title.nil?
      base_title
    else
      "#{base_title} | #{@title}"
    end
  end

  def base_url
    if root_url == 'http://localhost:3000/'
      'http://localhost'
    else 
      root_url[0..-2]
    end
  end

  def s_js_path
    base_url + ":8000"
  end

  def fullscreen?
    # Fullscreen on interview page.
    params['action'] == 'show' and params['controller'] == 'interviews' and signed_in?
  end
end
