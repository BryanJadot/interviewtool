module InterviewersHelper
  def recent_interviews(interviewer)

  end

  def possible_timezones()
    time_zones = (-720..840).step(15)
    time_zone_names = time_zones.collect do |time|
      ["UTC %+d:%02d" % [time / 60, time.modulo(60)], time]
    end
    return time_zone_names
  end
end
