module InterviewsHelper
  Error = Struct.new(:attribute, :error)

  def supported_lang_options
    return [["None", "none"], ["C", "c"], ["C++", "c++"], ["Java", "java"], ["Python", "python2"]]
  end

  def get_questions
    questions = current_user.questions.collect do |question|
      [question.question, question.id]
    end
  end

  def get_chat_html(chat)
    if chat.nil?
      return ''
    end

    chat_html = ""
    chat.split('\n').each do |line|
      (name, chat) = line.split('\t')
      chat_html = chat_html + '<b>' + name + ': </b>' + chat + '<br>'
    end

    return raw(chat_html)
  end

  def upcoming_interviews()
    return current_user.interviews.where("time_start >= :now", { :now => Time.now}).order("time_start ASC")
  end

  def previous_interviews(amount = -1)
    old_interviews = current_user.interviews.where("time_start < :now", { :now => Time.now}).order("time_start DESC")
    return old_interviews
  end

  def get_error_info(interviewee, interview)
    errors = interviewee.errors
    if errors.empty?
      errors = @interview.errors
    end
    attribute = errors.keys()[0]
    error = errors.full_message(attribute, errors.get(attribute)[0])

    e = Error.new
    e.attribute = attribute
    e.error = error

    return e

  end
end
