class QuestionsController < ApplicationController
  # GET /questions
  # GET /questions.json
  def index
    if signed_in?
      @questions = current_user.questions
      @question = Question.new
      respond_to do |format|
        format.html # index.html.erb
        format.json { render :json => @questions }
      end
    else
      redirect_to "/"
    end
  end

  # GET /questions/1
  # GET /questions/1.json
  def show
    @question = record_error_redir { Question.find(params[:id]) }

    if authenticate_id(@question.interviewer_id)
      respond_to do |format|
        format.html # show.html.erb
        format.json { render :json => @question }
      end
    end
  end

  # GET /questions/new
  # GET /questions/new.json
  def new
    if signed_in?
      @question = Question.new

      respond_to do |format|
        format.html # new.html.erb
        format.json { render :json => @question }
      end
    else
      redirect_to "/"
    end
  end

  # GET /questions/1/edit
  def edit
    @question = record_error_redir { Question.find(params[:id]) }

    authenticate_id(@question.interviewer_id)
  end

  # POST /questions
  # POST /questions.json
  def create
    puts params
    if signed_in?
      @question = Question.new(
                               :interviewer_id => current_user.id,
                               :question => params[:question_question],
                               :codedoc => params[:question_codedoc]
                              )

      respond_to do |format|
        if @question.save
          @text = @question.question
        end
        format.js
      end
    end
  end

  # PUT /questions/1
  # PUT /questions/1.json
  def update
    @question = record_error_redir { Question.find(params[:id]) }

    if (signed_in? and current_user.id == @question.interviewer_id)
      respond_to do |format|
        if @question.update_attributes(params[:question])
          format.html { redirect_to @question, :notice => 'Question was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render :action => "edit" }
          format.json { render :json => @question.errors, :status => :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /questions/1
  # DELETE /questions/1.json
  def destroy
    @question = record_error_redir { Question.find(params[:id]) }

    if signed_in? and current_user.id == @question.interviewer_id
      @question.destroy

      respond_to do |format|
        format.html { redirect_to questions_url }
        format.json { head :no_content }
      end
    end
  end
end
