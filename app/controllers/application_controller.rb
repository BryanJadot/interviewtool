class ApplicationController < ActionController::Base
  protect_from_forgery
  include SessionsHelper

  before_filter :set_user_time_zone

  private

  def set_user_time_zone
    Time.zone = current_user.time_zone if signed_in?
  end

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end

  def record_error_redir
    begin
      returns = yield
    rescue ActiveRecord::RecordNotFound
      not_found
    end
    return returns
  end
end
