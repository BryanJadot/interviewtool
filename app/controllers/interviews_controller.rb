class InterviewsController < ApplicationController
  helper_method :api_key
  helper_method :token

  require 'net/http'
  require 'uri'
  require 'juggernaut'
  require 'opentok'

  # GET /interviews
  # GET /interviews.json
  def index
    if authenticate
      @interviews = current_user.interviews
      @interview = Interview.new
      @interviewee = Interviewee.new

      respond_to do |format|
        format.html # index.html.erb
        format.json { render :json => @interviews }
      end
    end
  end

  # GET /interviews/1
  # GET /interviews/1.json
  def show
    @interview = record_error_redir { Interview.find(params[:id]) }
    interviewer = @interview.interviewer
    @is_interviewer = signed_in? && current_user?(interviewer)

    user = nil
    if @is_interviewer
      user = interviewer
    elsif params[:key] == @interview.key
      user = @interview.interviewee
    else
      deny_access
      return
    end

    @name = user.name
    @curr_answer_id = 0
    @curr_answer_text = ""
    @curr_answer_lang = "none"
    @curr_question = ""
    @questions_tags = []

    answers = @interview.answers
    if !answers.empty?
      @questions_tags = @interview.answers.collect do |answer|
        [answer.question.question, answer.id]
      end

      curr_answer = Answer.find(@interview.current_answer)
      @curr_question = curr_answer.question.question
      @curr_answer_id = curr_answer.id
      @curr_answer_text = curr_answer.answer
      @curr_answer_lang = curr_answer.language
    end

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @interview }
    end
  end

  # GET /interviews/new
  # GET /interviews/new.json
  def new
    if authenticate
      @interview = Interview.new
      @interviewee = Interviewee.new

      respond_to do |format|
        format.html # new.html.erb
        format.json { render :json => @interview }
      end
    end
  end

  # GET /interviews/1/edit
  def edit
    @interview = record_error_redir { Interview.find(params[:id]) }

    if authenticate_id(@interview.id)
      @interviewee = @interview.interviewee
    end
  end

  # POST /interviews
  # POST /interviews.json
  def create
    if signed_in?
      @interviewee = Interviewee.new(params[:interviewee])
      #new_min = params[:interview]["time_start(5i)"].to_i - (current_user.timezone - params[:dst].to_i)
      @interview = Interview.new(params[:interview])
      puts params
      @interview.interviewer_id = current_user.id
      @interview.rating = 0

      check = @interview.valid?
      check2 = @interviewee.valid?
      @valid = check && check2

      if @valid
        @interviewee.save

        # Generate a random key
        o =  [('a'..'z'),('A'..'Z'),('0'...'9')].map{|i| i.to_a}.flatten
        @interview.key = (0...16).map{o[rand(o.length)]}.join

        @interview.interviewee = @interviewee
        if !params[:interview_questions].nil?
          params[:interview_questions].each do |n|
            question = Question.find(n)
            question.interviews << @interview
            answer = Answer.new(:answer => question.codedoc, :language => "none")
            question.answers << answer
            @interview.answers << answer

            answer.save
          end

          @interview.current_answer = @interview.answers[0].id
        end

        # TOKBOX
        config_opentok
        session_properties = {OpenTok::SessionPropertyConstants::P2P_PREFERENCE => "enabled"}
        session = @opentok.create_session request.remote_addr, session_properties
        @interview.session_id = session.session_id
        #token = @opentok.generate_token :session_id => @session
        # end TokBox stuff

        @interview.save

        Thread.new do
          InterviewMailer.invite_interviewee(@interview).deliver
        end
      end

      respond_to do |format|
        format.js
      end
    end
  end

  # PUT /interviews/1
  # PUT /interviews/1.json
  def update
    @interview = record_error_redir { Interview.find(params[:id]) }

    if (signed_in? and @interview.interviewer_id == current_user.id)
      respond_to do |format|
        if @interview.update_attributes(params[:interview])

          if !params[:questions].nil?
            params[:questions].each do |n|
              question = Question.find(n)
              question.interviews << @interview
              @interview.questions << question
            end
          end
          @interview.save

          format.html { redirect_to @interview, :notice => 'Interview was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render :action => "edit" }
          format.json { render :json => @interview.errors, :status => :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /interviews/1
  # DELETE /interviews/1.json
  def destroy
    @interview = record_error_redir { Interview.find(params[:id]) }

    if (signed_in? and @interview.interviewer_id == current_user.id)
      @interview.interviewee.destroy
      @interview.answers.each do |answer|
        answer.destroy
      end

      @interview = Interview.find(params[:id])
      @interview.destroy

      respond_to do |format|
        format.html { redirect_to interviews_url }
        format.json { head :no_content }
      end
    end
  end

  # POST /interviews/save
  def save
    if params.has_key?(:interview_id)
      interview = record_error_redir { Interview.find(params[:interview_id]) }

      if !(signed_in? and interview.interviewer_id == current_user.id)
        return
      end

      if params.has_key?(:notes)
        interview.notes = params[:notes]
      end

      if params.has_key?(:curr_answer)
        interview.current_answer = params[:curr_answer]
      end

      interview.save
    end

    if params.has_key?(:answer_id)
      answer = record_error_redir { Answer.find(params[:answer_id]) }

      if !(signed_in? and answer.interview.interviewer_id == current_user.id)
        return
      end

      if params.has_key?(:answer)
        answer.answer = params[:answer]
      end

      if params.has_key?(:language)
        key = answer.interview.key
        Juggernaut.publish("/lang_#{key}", {:language => params[:language]})
        answer.language = params[:language]
      end

      answer.save
    end

    render :nothing => true
  end



  # POST /interviews/execute
  def execute
    post_uri = URI('http://10.252.14.246:8000/exec/899190/')
    begin
      response = Net::HTTP.post_form(post_uri, { :language => params[:lang], :code => params[:code], :room => params[:key] })
    rescue
      Juggernaut.publish("/code_#{params[:key]}", "Error executing code...\nPlease try again later.")
    end

    render :nothing => true
  end

  def receive_results
    if request.env['REMOTE_ADDR'] == '10.252.14.246'
      Juggernaut.publish("/code_#{params[:key]}", params[:output])

      render :nothing => true
    end
  end

  def change
    @answer = record_error_redir { Answer.find(params[:answer]) }
    interview = @answer.interview

    if (signed_in? and current_user.id == interview.interviewer_id)
      interview.current_answer = params[:answer]

      interview.save
      Juggernaut.publish("/answers_#{@answer.interview.key}", {:id => @answer.id, :code => @answer.answer, :question => @answer.question.question})

      respond_to do |format|
        format.js # change.js.erb
      end
    end
  end

  # Used to create a TokBox session.
  # api_key and token are shared with the view.
  private
  def api_key
    @api_key = 14618952
  end

  def api_secret
    @api_secret = "85f96b93e3a07a4f0af20a890612a3ea602fbba1"
  end

  def config_opentok
    if @opentok.nil?
      @opentok = OpenTok::OpenTokSDK.new api_key, api_secret
    end
  end

  def token(id)
    config_opentok
    @token = @opentok.generate_token :session_id => id
  end
end
