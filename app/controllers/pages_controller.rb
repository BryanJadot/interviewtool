class PagesController < ApplicationController
  def home
    if signed_in?
      redirect_to current_user
    end
    @interviewer = Interviewer.new
        
  end

end
