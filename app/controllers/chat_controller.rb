class ChatController < ApplicationController
  require 'juggernaut'
  require 'cgi'

  def chat
    message = CGI::escapeHTML(params[:chat_text])
    if message != ''
      name = params[:name]

      interview = Interview.find(params[:id])
      chat = interview.chat

      if interview.chat.nil?
        interview.chat = ''
      end

      interview.chat = interview.chat + name + '\t' + message + '\n';
      interview.save

      Juggernaut.publish("/chat_#{interview.key}", {:message => message, :name => name})
    end

    render :nothing => true
  end
end
