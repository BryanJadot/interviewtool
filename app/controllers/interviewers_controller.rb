class InterviewersController < ApplicationController
  # GET /interviewers
  # GET /interviewers.json
  def index
    @interviewers = Interviewer.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @interviewers }
    end
  end

  # GET /interviewers/1
  # GET /interviewers/1.json
  def show
    # For now, just redirect to the interviews page
    redirect_to '/interviews'
#    @interviewer = Interviewer.find(params[:id])

#    if authenticate_id(@interviewer.id)
#      respond_to do |format|
#        format.html # show.html.erb
#        format.json { render json: @interviewer }
#      end
#    end
  end

  # GET /interviewers/1/edit
  def edit
    @interviewer = record_error_redir { Interviewer.find(params[:id]) }

    authenticate_id(@interviewer.id)
  end

  # POST /interviewers
  # POST /interviewers.json
  def create
    @interviewer = Interviewer.new(params[:interviewer])

    @saved = @interviewer.save

    if @saved
      # Sign the user in
      sign_in @interviewer
    end

    respond_to do |format|
      format.js
    end
  end

  # PUT /interviewers/1
  # PUT /interviewers/1.json
  def update
    @interviewer = record_error_redir { Interviewer.find(params[:id]) }

    if current_user?(@interviewer)
      if params[:interviewer][:password].blank?
        params[:interviewer].delete(:password)
        params[:interviewer].delete(:password_confirmation)
      end

      respond_to do |format|
        # Check that current password field is matching before doing anything.
        if @interviewer == Interviewer.authenticate(@interviewer.email,
                                                    params[:interviewer][:current_password])
          #params[:interviewer].delete(:current_password)

          #params[:interviewer].delete(:password)
          #params[:interviewer].delete(:password_confirmation)

          # Always update the normal attributes
          @interviewer.name = params[:interviewer][:name]
          @interviewer.email = params[:interviewer][:email]
          @interviewer.time_zone = params[:interviewer][:time_zone]
          # Attempt to update password if the user gave an input.
          if !params[:interviewer][:password].blank?
            @interviewer.updating_password = true
            @interviewer.password = params[:interviewer][:password]
            @interviewer.password_confirmation = params[:interviewer][:password_confirmation]
          end

          # check if the update was successful
          if @interviewer.save
            # signing in interviewer again in case email or password were changed
            sign_in @interviewer
            format.html { render action: "edit", notice: 'Interviewer was successfully updated.' }
            # redirect_to account_settings_path
            format.json { head :no_content }
          else
            format.html { render action: "edit" }
            format.json { render json: @interviewer.errors, status: :unprocessable_entity }
          end
          # If the current password is wrong
        else
          format.html { render action: "edit" }
          format.json { render json: @interviewer.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /interviewers/1
  # DELETE /interviewers/1.json
  def destroy
    @interviewer = record_error_redir { Interviewer.find(params[:id]) }

    if current_user?(@interviewer)
      @interviewer.destroy

      respond_to do |format|
        format.html { redirect_to interviewers_url }
        format.json { head :no_content }
      end
    end
  end
end
