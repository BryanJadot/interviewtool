class InterviewMailer < ActionMailer::Base
  default from: "invite@interviewtool.com"

  SITE_URL = "http://www.coduru.com"

  def invite_interviewee(interview)
    interviewee = interview.interviewee

    @interviewee_name = interviewee.name
    @interviewer = interview.interviewer
    @interview = interview
    @site = SITE_URL

    mail(:to => interviewee.email,
         :subject => "Hi " + @interviewee_name + "! You have been invited to an interview")
  end
end
