// Editor for syntax highlighting.
var gEditors = {};

function setUpAce(editorID) {
  // Define editor global variable now.
  editor = ace.edit(editorID);
  editor.setReadOnly(false);
  editor.getSession().setUseSoftTabs(true);
  editor.getSession().setTabSize(4);
  editor.setTheme("ace/theme/textmate");

  gEditors[editorID] = editor;
}

function closeAce(editID) {
  gEditors[editID].destroy();
  $("#" + editID).empty();
}

function setUpShareJS(editorID, docName, path, initText) {
  console.log(docName);
  // Attach ShareJS to Ace editor
  sharejs.open(docName, 'text', path + '/channel',
      function(error, doc) {
      
    if (error) {
      console.error(error);
      return;
    }

    if (doc.created) {
      doc.insert(0, initText);
    }

    doc.attach_ace(gEditors[editorID]);
    gEditors[editorID].setReadOnly(false);
  });
}
