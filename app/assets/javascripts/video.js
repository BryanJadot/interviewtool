var MY_WIDTH = 220;
var MY_HEIGHT = 165;
var WIDTH = 220;
var HEIGHT = 165;

var apiKey;
var sessionId;
var token;
var imageURL;

function setInitVars(api_key, session_id, tok, image_url) {
    apiKey = api_key;
    sessionId = session_id;
    token = tok;
    imageURL = image_url;
}

var session = null;
var publisher = null;
var subscribers = {};

var deviceManager = null;

var isSubscriberVideoShowing = false;
var isMyVideoShowing = false;

// the divs
var subscriberParent = null;
var myVideoParent = null;
var audioButton = null;
var videoButton = null;
var buttonsParent = null;

var audioEnabled = false;
var videoEnabled = false;

var addedButtonsOnce = false;

var publisherPropsAudio = {
    width : MY_WIDTH,
    height : MY_HEIGHT,
    publishAudio : true,
    publishVideo : false
};

var publisherPropsVideo = {
    width : MY_WIDTH,
    height : MY_HEIGHT
};

var noMyVideoImage = null;
var noSubVideoImage = null;

var numRetries = 1;

$(window).load(function() {
    TB.setLogLevel(TB.DEBUG);
    TB.addEventListener("exception", exceptionHandler);

    function exceptionHandler(event) {
        alert("Exception: " + event.code + "::" + event.message);
    }

    // check the system req's and connect if they are fulfilled
    if(TB.checkSystemRequirements() != TB.HAS_REQUIREMENTS) {
        alert("You don't have the minimum requirements to run video chat." + "Please upgrade to the latest version of Flash.");
    } else {
        try {
            init();
        } catch (err) {
            // try again 1 more time
            if(numRetries > 0) {
                init();
            }

            numRetries--;
        }
    }

    subscriberParent = $('.subscriber_container');
    myVideoParent = $('.my_video_container');
    buttonsParent = $('#video_buttons');
    noMyVideoImage = $('#novideo_image2');
    noSubVideoImage = $('#novideo_image1');
    
    $("#buttons_loading").html(imageURL);
});

function init() {
    session = TB.initSession(sessionId);

    // Add event listeners to the session
    session.addEventListener('sessionConnected', sessionConnectedHandler);
    session.addEventListener('sessionDisconnected', sessionDisconnectedHandler);
    session.addEventListener('streamCreated', streamCreatedHandler);
    session.addEventListener('streamDestroyed', streamDestroyedHandler);

    connect();
}

function connect() {
    session.connect(apiKey, token);
}

function disconnect() {
    session.disconnect();
}

function sessionConnectedHandler(event) {
    // get the device manager once connected
    deviceManager = TB.initDeviceManager(apiKey);
    deviceManager.addEventListener("devicesDetected", devicesDetectedHandler);
    deviceManager.detectDevices();

    // Subscribe to streams currently in the Session
    for(var i = 0; i < event.streams.length; i++) {
        addStream(event.streams[i]);
    }
}

// HANDLERS

function devicesDetectedHandler(event) {
    // make sure to only add buttons once
    if(!addedButtonsOnce) {
        // get rid of the loading gif to replace it with buttons
        $('#buttons_loading').remove();
        
        // insert buttons into the page
        videoButton = $("<button class=\"btn\" id=\"video_toggle\"></button>");
        audioButton = $("<button class=\"btn\" id=\"audio_toggle\"></button>");
        buttonsParent.append(audioButton);
        buttonsParent.append(videoButton);

        addedButtonsOnce = true;

        if(event.cameras.length > 0 && event.microphones.length > 0) {
            setAudioToOn();
            setVideoToOn();
            audioEnabled = true;
            videoEnabled = true;
        } else if(event.cameras.length == 0 && event.microphones.length > 0) {
            disableVideoButton();
            setAudioToOn();
            audioEnabled = true;
        } else if(event.cameras.length > 0 && event.microphones.length == 0) {
            disableAudioButton();
            setVideoToOn();
            videoEnabled = true;
        } else {
            disableVideoButton();
            disableAudioButton();
        }
    }
}

// called when new streams are created in the session
function streamCreatedHandler(event) {
    // if we already have a subscriber
    if(isSubscriberVideoShowing) {
        return;
    }

    // Subscribe to the newly created streams
    for(var i = 0; i < event.streams.length; i++) {
        addStream(event.streams[i]);
    }
}

// the other guy stops showing us his video
function streamDestroyedHandler(event) {
    isSubscriberVideoShowing = false;
    
    // put the no video image back
    subscriberParent.append(noSubVideoImage);
}

// user got disconnected so reconnect to the session
function sessionDisconnectedHandler(event) {
    publisher = null;
    connect();
}

// BUTTON HELPERS

// disable the button, make it white, and make it says 'audio'
function disableAudioButton() {
    audioButton.attr('disabled', 'disabled');
    audioButton.attr('class', 'btn');
    audioButton.html('Audio');
}

// disable the button, make it white, and make it says 'video'
function disableVideoButton() {
    videoButton.attr('disabled', 'disabled');
    videoButton.attr('class', 'btn');
    videoButton.html('Video');
}

// enable the button, make it blue, and make it say 'audio on'
function setAudioToOn() {
    // enable the button
    audioButton.removeAttr('disabled');

    audioButton.attr('class', 'btn btn-success');
    audioButton.html('Audio On');
    audioButton.attr('onclick', 'startPublishingAudio()');
}

function setAudioToOff() {
    audioButton.attr('class', 'btn btn-inverse');
    audioButton.html('Audio Off');
    audioButton.attr('onclick', 'stopPublishing()');
}

function setVideoToOn() {
    // enable the button
    videoButton.removeAttr('disabled');

    videoButton.attr('class', 'btn btn-success');
    videoButton.html('Video On');
    videoButton.attr('onclick', 'startPublishingVideo()');
}

function setVideoToOff() {
    videoButton.attr('class', 'btn btn-inverse');
    videoButton.html('Video Off');
    videoButton.attr('onclick', 'stopPublishing()');
}

// Called when user wants to start publishing video to the session
function startPublishingVideo() {
    startPublishing(false);
}

// Called when user wants to start publishing video to the session
function startPublishingAudio() {
    startPublishing(true);
}

function startPublishing(isAudio) {
    // if we already have a video showing, dont show another one
    if(isMyVideoShowing) {
        return;
    }

    if(!publisher) {
        // create new div (child of my video container) to hold my video
        var newVideoDiv = document.createElement('div');
        newVideoDiv.setAttribute('id', 'opentok_publisher');
        
        // get rid of the image
        $('#novideo_image2').remove();
        
        // add the new div to the page
        myVideoParent.append(newVideoDiv);

        // pass the correct properties (video or just audio) to TokBox
        var publisherProps = null;
        if(isAudio) {
            publisherProps = publisherPropsAudio;
        } else {
            publisherProps = publisherPropsVideo;
        }

        publisher = session.publish(newVideoDiv.id, publisherProps);

        if(isAudio) {
            setAudioToOff();

            // if user can do video, dont let him do anything with video
            // button
            // after he turned on audio
            if(videoEnabled) {
                disableVideoButton();
            }
        } else {
            setVideoToOff();

            // if user can do audio, dont let him do anything with audio
            // button
            // after he turned on video
            if(audioEnabled) {
                disableAudioButton();
            }
        }

        isMyVideoShowing = true;
    }
}

// Called when user wants to stop their video or audio
function stopPublishing() {
    if(publisher) {
        session.unpublish(publisher);
    }

    publisher = null;

    // get rid of my video div
    myVideoParent.empty();
    
    // add back the no video image
    myVideoParent.append(noMyVideoImage);

    // if user can do both, let them do either again
    if(audioEnabled && videoEnabled) {
        setVideoToOn();
        setAudioToOn();
    }
    // if user can only do audio, let him turn it back on
    else if(audioEnabled && !videoEnabled) {
        setAudioToOn();
        disableVideoButton();
    }
    // if user can only do video
    else if(!audioEnabled && videoEnabled) {
        setVideoToOn();
        disableAudioButton();
    }

    isMyVideoShowing = false;
}

function addStream(stream) {
    // Check if this is the stream that I am publishing, and if so do not
    // add.
    if(stream.connection.connectionId == session.connection.connectionId) {
        return;
    }

    // if we already have a subscriber video showing
    if(isSubscriberVideoShowing) {
        return;
    }

    // create the div for the other guy's video
    var subscriberDiv = document.createElement('div');
    subscriberDiv.setAttribute('class', 'subscriber');
    subscriberDiv.setAttribute('id', stream.streamId);
    
    // get rid of the no video image
    $('#novideo_image1').remove();

    // add that div to the page
    subscriberParent.append(subscriberDiv);

    var subscriberProps = {
        width : WIDTH,
        height : HEIGHT
    };

    subscribers[stream.streamId] = session.subscribe(stream, subscriberDiv.id, subscriberProps);

    isSubscriberVideoShowing = true;
}