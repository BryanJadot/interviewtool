function resetInterviewerErrors() {
    $("#submit_feedback").html("");
    $("#new-interview form .errorable").removeClass("field-error");
    $("#error-message").html("<br /><br />").attr("class", "");
}

function setInterviewerErrors(attribute, error) {
    $("#interviewer_" + attribute).addClass("field-error");
    $("#error-message").html(error).attr("class", "error-space alert alert-error");
}
