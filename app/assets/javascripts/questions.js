function resetQuestionErrors() {
    $("#question_submit_feedback").html("");
    $("#new-question form .errorable").removeClass("field-error");
    $("#question-error-message").html("<br /><br />").attr("class", "error-space");
}

function setQuestionErrors(attribute, error) {
    $("#question_" + attribute).addClass("field-error");
    $("#question-error-message").html(error).attr("class", "alert alert-error error-space");
}
