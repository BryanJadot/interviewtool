// Set up code saving.
var delay = 3000;
var code_change_id = -1;  
var notes_change_id = -1;

function resetInterviewErrors() {
  $("#int_submit_feedback").html("");
  $("form .errorable").removeClass("field-error");
  $("#interview-error-message").html("<br /><br />").attr("class", "error-space");
}

function setInterviewErrors(attribute, error) {
  var attr = "";
  if (attribute == 'time_start') {
	  attr = '#interview_time_start';
  } else if (attribute == 'email') {
	  attr = '#interviewee_email';
  } else if (attribute == 'name') {
	  attr = '#interviewee_name';
  }

  $(attr).addClass("field-error");
  $("#interview-error-message").html(error).attr("class", "error-space alert alert-error");
}

function save_code() {
  var post_data = {};
  post_data["answer"] = gEditors['editor'].getSession().getValue();
  post_data["answer_id"] = $("#answer_id").val();

  save_fields(post_data);
}

function set_syntax_lang(lang) {
  if (lang == "none") {
    gEditors['editor'].getSession().setMode(new (require("ace/mode/text").Mode));
  } else if (lang == "c" || lang == "c++") {
    gEditors['editor'].getSession().setMode(new (require("ace/mode/c_cpp").Mode));
  } else if (lang == "java") {
    gEditors['editor'].getSession().setMode(new (require("ace/mode/java").Mode));
  } else if (lang == "python2") {
    gEditors['editor'].getSession().setMode(new (require("ace/mode/python").Mode));
  }
}

function save_fields(post_data) {
  $.post('/interviews/save', post_data);
}
    
function save_notes(interview_id) {
  var post_data = {};
  post_data["notes"] = $("#notes").val();
  post_data["interview_id"] = interview_id;
  save_fields(post_data);
}

function changeQuestion(data) {
  if("id" in data) {
    $(".current-question").text(data["question"]);
    $("#answer_id").val(data["id"]);
  }
}

function onUnload(interview_id) {
  save_code();
  save_notes(interview_id);
  
  // video
  disconnect();
}

function onLangChange() {
  var post_data = {};
  var lang = $("#languages").val();
  post_data["language"] = lang;
  post_data["answer_id"] = $("#answer_id").val();
  
  // Enable or disable code exec button based on lang selection
  if (lang == "none") {
    $("#code_exec").attr("disabled", "disabled");
  } else {
    $("#code_exec").removeAttr("disabled");
  }

  set_syntax_lang(lang);
  save_fields(post_data);
}

function onExecuteCode(key) {
  var post_data = {};
  post_data["lang"] = $("#languages").val();
  post_data["code"] = gEditors['editor'].getSession().getValue();
  post_data["key"] = key;
  $.post("/interviews/execute/", post_data); 
}

function onEditorChange() {
  if (code_change_id != -1) {
    clearTimeout(code_change_id);
  }

  code_change_id = setTimeout(function() { save_code(); }, delay);
}

function onNotesChanged(interview_id) {
  if(notes_change_id != -1) {
    clearTimeout(notes_change_id);
  }
  //alert(delay);
  notes_change_id = setTimeout(function() { save_notes(interview_id); }, delay);
}

function scroll_to_end_of_chat() {
  var numChat = $("#chat_hist option").size();
  var sizeChat = $("#chat_hist").attr("size");

  var scrollHeight = $("#chat_hist").prop("scrollHeight");
  $("#chat_hist").scrollTop(scrollHeight);
  $("#chat_hist").scrollTop($("#chat_hist").scrollTop());
}

function handleChat(data) {
  var prev_val = $('#chat_hist').html();
  var new_val = prev_val + "<b>" + data["name"] + ":</b> " + data["message"] + "<br />";
  $('#chat_hist').html(new_val);
  scroll_to_end_of_chat();
}
