# == Schema Information
#
# Table name: questions
#
#  id             :integer         not null, primary key
#  interviewer_id :integer
#  question       :string(255)
#  codedoc        :string(255)
#  created_at     :datetime        not null
#  updated_at     :datetime        not null
#

class Question < ActiveRecord::Base
  belongs_to :interviewer
  has_and_belongs_to_many :interviews
  has_many :answers

  validates :question,       :presence => true,
                             :length   => { :maximum => 140 }

  validates :interviewer_id, :presence => true
  attr_accessible :question, :interviewer_id, :codedoc
end
