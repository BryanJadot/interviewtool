# == Schema Information
#
# Table name: interviewers
#
#  id                 :integer         not null, primary key
#  email              :string(255)
#  created_at         :datetime        not null
#  updated_at         :datetime        not null
#  salt               :string(255)
#  encrypted_password :string(255)
#  email_verified     :boolean
#  time_zone          :string(255)
#  name               :string(255)
#

require 'digest'

class Interviewer < ActiveRecord::Base
    # Password stored virtually, not written in cleartext to the database
    attr_accessor :password, :updating_password

    # Accessible attributes
    attr_accessible :name, :email, :password, :time_zone

    # Regular expression to verify for a valid email syntax.
    email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

    has_many :interviews
    has_many :questions

    # Verifying fields

    validates :name, :length => { :maximum => 50 },
                     :presence => true

    # ensures email exists, has correct syntax and is unique
    validates :email, :presence => true,
                      :format => { :with => email_regex },
                      :uniqueness => { :case_sensitive => false }

    # Ensures password exists, is within range and has a confirmation attribute
    # if should_validate_password?
        validates :password, :presence       => true,
                             :length         => { :within => 6..40 }, :if => :should_validate_password?

  # validates_presence_of :password if :should_validate_password?

 # Make sure we encrypt and store the password before we write to the database
 # if we are updating the password
    before_save :encrypt_password, :if => :should_validate_password?


  # Returns true if the password is set to be changed or if it's the creation of a user
  def should_validate_password?
    updating_password || new_record?
  end

  # Return true if the submitted password matches the submitted password
  def has_password?(submitted_password)
    encrypted_password == encrypt(submitted_password)
  end

  def self.authenticate(email, submitted_password)
    user = find_by_email(email)
    return nil if user.nil?
    return user if user.has_password?(submitted_password)
  end

  def self.authenticate_with_salt(id, cookie_salt)
    user = find_by_id(id)
    (user && user.salt == cookie_salt) ? user : nil
  end

  def email_verified?
    email_verified
  end


  private
  def encrypt_password
    self.salt = make_salt unless has_password?(password)
    self.encrypted_password = encrypt(password)
  end

  def encrypt(string)
    secure_hash("#{salt}--#{string}")
  end

  def make_salt
    secure_hash("#{Time.now.utc}--#{password}")
  end

  def secure_hash(string)
    Digest::SHA2.hexdigest(string)
  end
end
