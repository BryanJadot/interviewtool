# == Schema Information
#
# Table name: interviews
#
#  id             :integer         not null, primary key
#  interviewer_id :integer
#  interviewee_id :integer
#  time_start     :datetime
#  chat           :string(255)
#  notes          :string(255)
#  created_at     :datetime        not null
#  updated_at     :datetime        not null
#  key            :string(255)
#  current_answer :integer
#  rating         :integer
#

class Interview < ActiveRecord::Base
  belongs_to :interviewer
  belongs_to :interviewee

  has_and_belongs_to_many :questions
  has_many :answers

  validates :time_start, :presence => true

  attr_accessible :interviewee_id, :time_start, :interviewer_id, :questions, :rating
  validates_numericality_of :rating, :only_integer => true, :message => "can only be whole number."
  validates_inclusion_of :rating, :in => 0..5, :message => "can only be between 0 and 5."
end

