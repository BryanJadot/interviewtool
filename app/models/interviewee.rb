class Interviewee < ActiveRecord::Base
  has_many :interviews

  email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  validates :email, :presence => true,
                    :format => { :with => email_regex }

  validates :name, :presence => true
end
# == Schema Information
#
# Table name: interviewees
#
#  id         :integer         not null, primary key
#  email      :string(255)
#  notes      :string(255)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#  name       :string(255)
#

