class Answer < ActiveRecord::Base
  belongs_to :interview
  belongs_to :question
end
# == Schema Information
#
# Table name: answers
#
#  id           :integer         not null, primary key
#  answer       :text
#  created_at   :datetime        not null
#  updated_at   :datetime        not null
#  interview_id :integer
#  question_id  :integer
#  language     :string(255)
#

