InterviewTool::Application.routes.draw do

'coduru.com'.tap do |host|
    constraints(:host => host) do
      root :to => redirect {|params, req| URI.escape(req.query_string.present? ? "#{req.protocol}www.#{host}?#{req.query_string}" : "#{req.protocol}www.#{host}") }
      match '/*path', :to => redirect {|params, req| URI.escape(req.query_string.present? ? "#{req.protocol}www.#{host}/#{params[:path]}?#{req.query_string}" : "#{req.protocol}www.#{host}/#{params[:path]}") }
    end
end

  resources :interviewers,  :only => [:create, :show, :edit, :destroy, :update]

  # Block update until I can fix it
  resources :interviews, :only => [:create, :show, :destroy, :index]
  resources :sessions,      :only => [:create, :destroy]

  # Block edit and show until I can fix it
  resources :questions, :only => [:create, :destroy, :index]

  match '/interviews/save', :to => 'interviews#save'
  match '/account_settings',      :to => 'interviewers#edit'
  match '/signout',       :to => 'sessions#destroy'
  match '/chat',          :to => 'chat#chat'
  match '/interviews/execute', :to => 'interviews#execute'
  match '/interviews/receive_results', :to => 'interviews#receive_results'
  match '/interviews/change', :to => 'interviews#change'

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  #match '/', :to => 'pages#home'
  root :to => 'pages#home'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
  
end
