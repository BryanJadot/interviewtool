class RemoveNameFromInterviewer < ActiveRecord::Migration
  def up
    remove_column :interviewers, :first_name
    remove_column :interviewers, :last_name

    add_column :interviewers, :name, :string
  end

  def down
    remove_column :interviewers, :name

    add_column :interviewers, :last_name, :string
    add_column :interviewers, :first_name, :string
  end
end
