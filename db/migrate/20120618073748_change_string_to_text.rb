class ChangeStringToText < ActiveRecord::Migration
  def up
    change_column :interviewees, :notes, :text, :limit => nil
    change_column :interviews, :notes, :text, :limit => nil
    change_column :interviews, :chat, :text, :limit => nil
    change_column :questions, :question, :text, :limit => nil
    change_column :questions, :codedoc, :text, :limit => nil
  end

  def down
    change_column :interviewees, :notes, :string
    change_column :interviews, :notes, :string
    change_column :interviews, :chat, :string
    change_column :questions, :question, :string
    change_column :questions, :codedoc, :string
  end
end
