class InterviewsQuestions < ActiveRecord::Migration
  def up
    create_table :interviews_questions, :id => false do |t|
      t.integer :interview_id
      t.integer :question_id
    end
  end

  def down
    drop_table :interviews_questions
  end
end
