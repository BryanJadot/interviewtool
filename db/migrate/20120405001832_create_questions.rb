class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.integer :interviewer_id
      t.string :question
      t.string :codedoc

      t.timestamps
    end
  end
end
