class AddTimeZoneToInterviewers < ActiveRecord::Migration
  def change
    add_column :interviewers, :time_zone, :string
  end
end
