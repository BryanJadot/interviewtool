class AddSaltToInterviewer < ActiveRecord::Migration
  def change
    add_column :interviewers, :salt, :string

    add_column :interviewers, :encrypted_password, :string

    add_column :interviewers, :email_verified, :boolean

  end
end
