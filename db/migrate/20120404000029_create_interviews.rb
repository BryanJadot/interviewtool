class CreateInterviews < ActiveRecord::Migration
  def change
    create_table :interviews do |t|
      t.integer :interviewer_id
      t.integer :interviewee_id
      t.datetime :time_start
      t.string :chat
      t.string :notes

      t.timestamps
    end
  end
end
