class CreateInterviewees < ActiveRecord::Migration
  def change
    create_table :interviewees do |t|
      t.string :email
      t.string :notes

      t.timestamps
    end
  end
end
