# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120618073748) do

  create_table "answers", :force => true do |t|
    t.text     "answer"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "interview_id"
    t.integer  "question_id"
    t.string   "language"
  end

  create_table "interviewees", :force => true do |t|
    t.string   "email"
    t.text     "notes"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "name"
  end

  create_table "interviewers", :force => true do |t|
    t.string   "email"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "salt"
    t.string   "encrypted_password"
    t.boolean  "email_verified"
    t.string   "time_zone"
    t.string   "name"
  end

  create_table "interviews", :force => true do |t|
    t.integer  "interviewer_id"
    t.integer  "interviewee_id"
    t.datetime "time_start"
    t.text     "chat"
    t.text     "notes"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.string   "key"
    t.string   "session_id"
    t.integer  "current_answer"
    t.integer  "rating"
  end

  create_table "interviews_questions", :id => false, :force => true do |t|
    t.integer "interview_id"
    t.integer "question_id"
  end

  create_table "questions", :force => true do |t|
    t.integer  "interviewer_id"
    t.text     "question"
    t.text     "codedoc"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

end
